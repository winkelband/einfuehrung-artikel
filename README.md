# Einführung zum Verfassen von Artikeln

akarchwiss.de

## Installation

1. chocolatey
2. git (git bash), auf CRLF -> LF achten; git bringt OpenSSH unter Windows mit
3. hugo-extended
5. Atom, ggf. Zettlr
6. choco install git atom zettlr -confirm
7. go → zip-Archiv, dann Umgebungsvariable; alternativ: Installer mit Administrationsrechten

Git konfigurieren:

* `git config --global core.autocrlf true`
* `git config --global user.name "John Doe"`
* `git config --global user.email johndoe@example.com`

`ssh-keygen -f ~/.ssh/gitlab-<username> -t ed25519 -C "your_email@example.com"`

Konfigurationsdatei erstellen: ~/.ssh/config:

```bash
Host gitlab.com
    HostName gitlab.com
    User <username>
    IdentityFile ~/.ssh/gitlab-<username>
```

Permission setzen: `chmod 644 ~/.ssh/config`

SSH-Public-Key in GitLab eintragen

Neue Git-Bash starten und in einem Arbeitsverzeichnis git clone ausführen: `git clone git@gitlab.com:akarchwiss/akarchwiss.de.git`
ggf. ssh-agent starten

## Editor

### Atom

#### Empfohlene Plug-Ins (Atom Packages)

* Autovervollständigung für Dateifpade: https://atom.io/packages/autocomplete-paths
* Bilddateien direkt in Atom betrachten: https://atom.io/packages/image-view

#### Kommentare im Fließtext

Du kannst im Fließtext Kommentare setzen, die nicht im HTML gerendert werden.
Du kannst den Text damit in einzelne Abschnitte gliedern oder deine Teammitglieder auf etwas hinweisen:

```markdown
<!-- Literaturangaben -->

<!-- Literaturangaben Ende -->

<!-- Fußnoten -->
```

Beachte: Wenn Du ein Hugo-Archetypes nutzt, erhältst Du automatisch eine für die jeweilige Rubrik (Reguläre Posts/Events) strukturierte und kommentierte Vorlage, in die Du deinen Text setzen kannst.

#### Kurzusammenfassung

Um auf den Kategorieseiten, die unsere Posts auflisten, eine Kurzzusammenfassung vom Artikel anzuzeigen, kannst Du den Texttrenner `<!--more-->` zwischen zwei *Absätze* setzen:

```markdown
Am 13. Mai 2018 war es Vereinsmitgliedern möglich, sich mit einem Informationsstand an der Veranstaltung »Geschichte(n) erleben« des archäologischen Freilichtmuseums Zeiteninsel in Weimar-Argenstein zu beteiligen.

<!--more-->

Im Zuge dessen bewarben die Vereinsmitglieder den Arbeitskreis [...]
```
**<SCREENSHOT VON ERGEBNIS EINFÜGEN**

Wenn Du stattdessen eine vom Fließtext unabhängige Zusammenfassung verwenden möchtest,
so kannst Du diese in den Front Matter als Parameter `summary` schreiben:

```yaml
summary:
	>- Am 8.10.2017 fand im Vonderau Museum (Fulda) die 1. Tagung des AkArchWiss statt.
```

Beachte: Alle Auszeichnungen (z. B. kurisver Text) und Hyperlinks werden in unformatierten Text für die Kurzzusammenfassung umgewandelt, bleiben im eigentlichen Artikel jedoch erhalten.

#### Überschriften

Da der Titel und ggf. Untertitel in einem Post bereits als 1. und 2. Überschrift gesetzt werden,
solltest Du dich auf die Verwendung der Überschrifthierarchienen 3 bis 6 beschränken.

```markdown
### Überschrift 3

... Fließtext ...

#### Überschrift 4

... Fließtext ...

##### Überschrift 5

... Fließtext ...

###### Überschrift 6

... Fließtext ...
```

#### Auszeichnungen

Für Kursiv- und Fettdruck steht dir die [Markdown-Syntax](https://markdown) zur Verfügung:

```markdown
Ich bin ein *kursivgedruckter Text.* Ich bin wieder in normalgedruckter Text.

Ich bin ein **fettgedruckter Text.** Ich bin wieder in normalgedruckter Text.
```

Beachte: Du kannst diese Auszeichnungen auch auf andere Markdown-Syntax (bspw. Hyperlinks) anwenden.

#### Hyperlinks

Interne Hyperlinks verweisen auf eine Unterseite von akarchwis.de.
Externe Hyperlinks führen auf eine andere Domain und sind auf unserer Website durch ein kleines Icon gekennzeichnet.

Jeder Hyperlink sollte um einen Titeltext ergänzt werden.

##### Interne Hyperlinks

Setze kein `https://`, sondern nutze eine relative Pfadangabe zur Domain,
um die Unterseite *im selben Browserfenster* öffnen zu lassen:

```markdown
Die Kurzfassung zu [Teil 1](/berichte/2020/exkursion-rottenburg-1 "Rottenburg I").
```

Beachte: PDFs, die von akarchwiss.de stammen, werden in einem neuen Fenster/Tab geöffnet.

##### Externe Hyperlinks

Setze ein `https://` oder `http://` vor den restlichen Teil der URL,
um die Seite *in einem neuen Tab oder Fenster* öffnen zu lassen:

```markdown
Herr Runzheimer arbeitete mit Erzen aus der [Grube Fortuna](https://grube-fortuna.de/ "Geowelt Fortuna e. V.").
```

#### Inschriften

```markdown
{{< epigraph title="Inschrift 1" >}}

{{< transcription >}}
**DEO** MERCVRIO VI / SVCIO E SA(N)CT(A)E VISV / 
CI(A)E P(UBLIUS) QUARTIONIVS SECUNDINVS DECV(RIO) / [C]IVI(TATIS) SUMAL(OCENNENSIS) EX IV(SSU) V(OTUM) S(OLVIT) L(IBENS) M(ERITO)
{{< /transcription >}}

{{< translation >}}
Dem Gott Mercurius Visucius und der heiligen Visucia
hat Publius Quartonius Secundinus, Ratsmitglied der Gebietskörperschaft Sumelocenna, auf Geheiß sein Gelübde eingelöst gerne aufgrund der [göttlichen] Leistung.
{{< /translation >}}

{{< /epigraph >}}
```

Beachte: Du kannst Auszeichnungen (Kursiv-/Fettdruck) verwenden.
Neben einfachen mehrzeiligen Transkriptionen/Übersetzungen sind auch weiche und harte Zeilenumbrüche möglich. 

#### Zitate

Wenn Du ein längeres Zitat hervorben möchtest,
nutze die folgende Markdown-Syntax:

```markdown
> Ich bin ein umfangreiches Zitat.
```

#### Auflistungen

```markdown
### Warum solltest Du beitreten?

* weil Du dich für kulturhistorische Themen interessierst.
* weil Du dich aktiv im Vereinsleben einbringen möchtest.
* weil Du gerne Exkursionen zu historischen Denkmälern oder Museen machst.
* weil Du Lust auf kulturhistorische Veranstaltungen hast und dich auch selbst bei solchen einbringen möchtest.
	+ untergeordneter Punkt einer verschachtelten Liste (nicht mehr als 2 Ebenen gesamt nutzen)

Bei weiteren Fragen steht Dir unser Neumitgliederbetreuer ...
```

Beachte: Vor und nach einem Auflistungsblock musst Du eine Leerzeile setzen, wenn weiterer Text folgt.

#### Tabellen

Nutze die Markdown-Syntax für das Schreiben von Tabellen.
In der Tabelle selbst sind die gewohnten Auszeichnungen (Kursiv-/Fettdruck) möglich:

```markdown
| Uhrzeit   | Programmpunkt |
|-----------|---------------|
| 08:30     | Start *Frankoniabrunnen am Residenzplatz, Würzburg* |
```

Beachte: Programme wie Zettlr können automatisch Zellen und Zeilen einer Tabelle in der Markdown-Syntax anlegen.

#### Fußnoten

Um Fußnoten in einem Artikel zu verwenden,
nutze die Markdown-Syntax, Du kannst im selben Text auf eine Fußnote beliebig oft verweisen.
Auf der Website werden Fußnoten als Nummern hinter der markierten Stelle gesetzt. Beim Überfahren der Nummer mit der Maus wird der Fußnotentext präsentiert. Sehr lange Fußnoten sind scrollbar.

```
[...]

Am Ende des Prozesses sollte hier die »Schlacke«[^Schlacke] aus dem Ofeninneren abfließen.

[...]

Den Ofen galt es langsam auf die richtige Temperatur zu bringen. Danach wurde das zuvor von »taubem Gestein«[^taubes Gestein] gelöste Erz in den Rennofen gegeben. 

[...]

<!-- Fußnoten -->
[^Schlacke]: Die nichtmetallischen Rückstände, die im Verlaufe des Verhüttungsprozess anfallen.
[^taubes Gestein]: Gestein, das kaum Erze enthält.
```

Beachte: Setze keine Bilder in die Fußnoten!

### Bibliografische Angaben

```markdown
<!-- Literaturangaben -->
{{< bibliography title="Lesenswertes" >}}

{{< bib-item
    author="Gairhos, Sebastian"
    item="Stadtmauer und Tempelbezirk von Sumelocenna. Die Ausgrabungen von 1995 bis 99 in Rottenburg am Neckar, Flur »Am Burggraben«, in: Regierungspräsidium Stuttgart – LAD, Esslingen am Neckar (Hrsg.): Forschungen und Berichte zur Vor- und Frühgeschichte in Baden-Württemberg, Bd. 104, Stuttgart 2008">}}
{{< bib-item
  author="Heiligmann, Karin"
  item="Sumelocenna – Römisches Stadtmuseum Rottenburg am Neckar, in: Landesdenkmalamt Baden-Württemberg/Förderkreis für die ur- und frügeschichtliche Forschung in Baden/Gesellschaft für Vor- und Frühgeschichte in Württemberg und Hohenzollern (Hrsg.): Führer zu archäologischen Denkmälern in Baden-Württemberg, Bd. 18, Stuttgart 1992">}}
{{< bib-item
    author="Heising, Alexander"
    item="Sumelocenna/Rottenburg am Neckar – Neues und (Alt-)Bekanntes zu einer römischen »Stadt«. Ein Gedächtnisaufschrieb des Vortrags Alexander Heisings durch Peter Ehrmann vom Stadtarchiv Rottenburg, in: Sülchgauer Altertumsverein e. V. Rottenburg am Neckar:  Eigenspuren. Archäologie und Regionalgeschichte um Rottenburg, Bd. 60/61, Jg. 2016/2017, Rottenburg am Neckar 2016, S. 7–8">}}

{{< /bibliography >}}
<!-- Literaturangaben Ende -->
```

Beachte: Setze weder einen Doppelpunkt nach der Autorenschaft noch einen abschließenden Punkt nach den Titelangaben/Seitenzahlen.

#### Fotogalerien

Mehrere kleinere Fotogalerien sollen den Fließtext auflockern und sollten nach Möglichkeit jeweils thematisch verwandte Fotos zeigen (z. B. ein Arbeitsvorgang bei einem Experiment).

Relative Pfade:

```markdown
<!-- Fotogalerie 1 -->
{{< gallery >}}

	{{< figure
		link="images/2018-05-13-akarchwiss-zeiteninsel-2018-infostand.jpg" title="Informationsstand"
		caption="Der AkArchWiss baute sich mit seinem Informationsstand bei Herrn Runzheimer auf, den es zu unterstützen galt."
	>}}
	{{< figure
		link="images/2018-05-12-akarchwiss-zeiteninsel-2018-rennofen-1.jpg"
		title="Rennofen bei Nacht"
		caption="Der Rennofen wurde am Mittag des 12. Mai auf Temperatur gebracht und über Nacht betrieben."
	>}}
	{{<figure
		link="images/2018-05-12-akarchwiss-zeiteninsel-2018-schueren-1.jpg"
		caption="Dies bedeutete: Nachlegen von Holz, Schüren und beständig Luft zuzuführen."
	>}}
	
{{< /gallery >}}
<!-- Fotogalerie 1 Ende -->
```

Beachte: Die Zeilenumbrüche dienen hier der Übersichtlichkeit und sind nicht funktional.

#### Absätze

Absätze trennst Du mit einer Leerzeile.

```markdown
Zudem beantworteten die Vereinsmitglieder Fragen der Besucher auf der Zeiteninsel, wenn die Betreuung des Rennofens selbiges für Herrn Runzheimer unmöglich machte.

Herr Runzheimer arbeitete mit Erzen aus der [Grube Fortuna.](https://grube-fortuna.de/ "Geowelt Fortuna e. V.")
```

#### Soft Carriage Return/Weicher Zeilenumbruch

Zwei Leerzeichen am Ende der Zeile, dann Carriage Return.

```
Inschriften bsp  
Zeile  
Zeile
```

#### E-Mails

#### CTA-Buttons

### Frontmatter

* Metadaten zum Post
