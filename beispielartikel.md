```markdown
---
title: »Geschichte(n) erleben« 2018
subtitle: "Vortragsbewerbung und Rennofenbau auf der Zeiteninsel"

publishDate: 2020-05-24T12:00:08+00:00

author:
  - mscheck

tags:
  - Experimentelle Archäologie
  - Rennofen
  - Eisenverhüttung
  - Zeiteninsel
  - Weimar-Argenstein

thumb_img: images/zeiteninsel-featured.jpg
header_img: images/2018-05-12-akarchwiss-zeiteninsel-2018-schueren-1.jpg
header_mobile_img: images/2018-05-13-akarchwiss-zeiteninsel-2018-infostand.jpg

featured: true
weight: 1
featured_headline: Zeiteninsel 2018
featured_img: /lesefunde/2020/zeiteninsel-geschichte-erleben/images/zeiteninsel-featured.jpg
featured_img_alt: "Rennofen bei Nacht"

layout: post 
---

Am 13. Mai 2018 war es Vereinsmitgliedern möglich, sich mit einem Informationsstand an der Veranstaltung »Geschichte(n) erleben« des archäologischen Freilichtmuseums Zeiteninsel in Weimar-Argenstein zu beteiligen.

<!--more-->

Im Zuge dessen bewarben die Vereinsmitglieder den Arbeitskreis sowie zwei Fachvorträge, die am 6. Juni und 3. Juli 2018 stattfinden würden, mit Flyern und Bildern früherer Exkursion. Darüber hinaus halfen die Mitglieder Herrn M. Runzheimer, der sich an der Eisengewinnung mit einem eigenen Nachbau eines eisenzeitlichen Rennofens versuchte. Hierbei erhielt er Unterstützung bei der Brennholzgewinnung und Befeuerung des Ofens. Zudem beantworteten die Vereinsmitglieder Fragen der Besucher auf der Zeiteninsel, wenn die Betreuung des Rennofens selbiges für Herrn Runzheimer unmöglich machte.

Herr Runzheimer arbeitete mit Erzen aus der [Grube Fortuna.](https://grube-fortuna.de/ "Geowelt Fortuna e. V.") Er fertigte einen leicht kegelförmigen Rennofen an. Der Ofenschacht war aus Lehmziegeln angelegt, die außen mit Lehm beschichtet wurden. Die Bauzeit eines solchen Ofens schätzte Herr Runzheimer auf einen halben bis vollen Tag. Der Ofen besaß drei Öffnungen: Neben der oberen Schachtöffnung, befanden sich zwei kleinere Öffnungen am unteren Teil des Ofens. Eine davon – an der Vorderseite des Rumpfes – war mit Ziegeln verstärkt; sie wird im Folgenden als ›Eingang‹ bezeichnet. Über den Eingang erfolgte die Befeuerung des Ofens. Am Ende des Prozesses sollte hier die »Schlacke«[^Schlacke] aus dem Ofeninneren abfließen. Vor dem Eingang hatte Herr Runzheimer ein, mit Steinen umrandetes und mit gebrochenem Lehm ausgefülltes Auffangbecken für die Schlacke angelegt. Über die hintere Öffnung, die als Düse angelegt war, erfolgte eine permanente Luftzufuhr. Bei dieser Präsentation wurde die Luftzufuhr des Rennofens anachronistisch mit einem elektrischen Lüfter betrieben, statt mit einem Blasebalg. Dies geschah, damit die Luftzufuhr trotz des »Tages der offenen Tür« konstant aufrechterhalten werden konnte und sich somit ein gutes Ergebnis erzielen ließ.Dadurch war es etwa möglich, Fragen zum Ofen, unabhängig von der ansonsten notwendigen Betätigung des Blasebalgs, zu beantworten.

<!-- Fotogalerie 1 -->
{{< gallery >}}
	 {{< figure link="images/2018-05-13-akarchwiss-zeiteninsel-2018-infostand.jpg"
      title="Informationsstand"
      caption="Der AkArchWiss baute sich mit seinem Informationsstand bei Herrn Runzheimer auf, den es zu unterstützen galt." >}}
	 {{< figure link="images/2018-05-12-akarchwiss-zeiteninsel-2018-rennofen-1.jpg"
      title="Rennofen bei Nacht"
      caption="Der Rennofen wurde am Mittag des 12. Mai auf Temperatur gebracht und über Nacht betrieben." >}}
   {{<figure link="images/2018-05-12-akarchwiss-zeiteninsel-2018-schueren-1.jpg"
     caption="Dies bedeutete: Nachlegen von Holz, Schüren und beständig Luft zuzuführen." >}}
   {{<figure link="images/2018-05-13-akarchwiss-zeiteninsel-2018-schueren-2.jpg"
     caption="Auch am Veranstaltungstag wird geschürt und belüftet." >}}
   {{<figure link="images/2018-05-13-akarchwiss-zeiteninsel-2018-rennofen-2.jpg"
     title="Ergebnisse bergen"
     caption="Herr Runzheimer öffnete den Ofen, um die Schlacke ablaufen zu lassen." >}}
   {{<figure link="images/2018-05-13-akarchwiss-zeiteninsel-2018-funkenflug.jpg"
     caption="Funken stoben, die Schlacke lief jedoch nicht heraus …" >}}
{{< /gallery >}}
<!-- Fotogalerie 1 Ende -->

Den Ofen galt es langsam auf die richtige Temperatur zu bringen. Danach wurde das zuvor von »taubem Gestein«[^taubes Gestein] gelöste Erz in den Rennofen gegeben. Der Ofen musste vom Mittag des Vortags und über Nacht auf den 13. Mai beheizt werden, um bei der Eisengewinnung erfolgreich sein zu können. Durch diese dauerhafte Erhitzung, erreichte der Rennofen Temperaturen zwischen 1250 und 1450 °C. Dabei reduzierte sich das Gestein auf das Roheisen. Dessen Schmelztemperatur von 1536 °C durfte nicht erreicht werden, da sich ansonsten zum Schmieden ungeeignetes Gusseisen gebildet hätte. Das Roheisen sammelte sich an der Stelle mit der höchsten Temperatur — also an der Stelle, von der der Ofen befeuert wurde — zur sogenannten Luppe bzw. zum sogenannten Eisenschwamm.  
Zum Ende der Veranstaltung, gegen 17 Uhr, konnte sich Herr Runzheimer daran machen die Ergebnisse zu sichten. Hierzu öffnete er zunächst den Ofeneingang, der für den Veranstaltungstag mit einem Ziegel und Lehm verschlossen worden war, um die Schlacke abfließen zu lassen. Diese war jedoch nicht flüssig genug, sodass der Ofen komplett abgebaut, genauer gesagt, in seine Einzelteile zerlegt werden musste. Das ging mit Hammer, Stemmeisen, Zange und Eisenstange sowie Hitzeschutzkleidung vonstatten und dauerte etwa 20 Minuten.  
Der Ofen war stellenweise zu heiß geworden, denn es ließ sich während des Abbaus auch ein Stück Gusseisen bergen.
Im unteren Bereich des Ofens vor dem Eingang hatte sich Luppe gesammelt, die noch in den Resten des Ofens und ein weiteres Mal auf einem Holzklotz behauen wurde, um sie von der Schlacke zu trennen. Im Verhüttungsprozess war ein beträchtlicher Klumpen Luppe entstanden. Um nutzbares Eisen daraus zu gewinnen, muss die Luppe in einem Schmiedeprozess zunächst mehrfach erhitzt, ausgeschmiedet und umgefalten werden.

<!-- Fotogalerie 2 -->
{{< gallery >}}
{{<figure link="images/2018-05-13-akarchwiss-zeiteninsel-2018-rennofen-3.jpg"
  title="Abbau"
  caption="… Daher musste der Rennofen abgebaut werden, um an die Luppe zu gelangen, die sich unten im Ofen befand. Dabei kamen die Ziegel aus dem Inneren des Ofens zu Tage." >}}
{{<figure link="images/2018-05-13-akarchwiss-zeiteninsel-2018-rennofen-4.jpg"
  caption="Stück für Stück wird der Rennofen zerkleinert …" >}}
{{<figure link="images/2018-05-13-akarchwiss-zeiteninsel-2018-gusseisen.jpg"
  caption="… Dabei findet sich auch ein Stück Gusseisen — ein unerwünschtes Nebenprodukt. Der Rennofen ist zu heiß geworden und hat das Roheisen zum Schmelzen gebracht." >}}
{{<figure link="images/2018-05-13-akarchwiss-zeiteninsel-2018-rennofen-5.jpg"
  caption="Der Rennofen verschwindet, …" >}}
{{<figure link="images/2018-05-13-akarchwiss-zeiteninsel-2018-rennofen-6.jpg"
  caption="… ist nur noch zur Hälfte vorhanden." >}}
{{< /gallery >}}
<!-- Fotogalerie 2 Ende -->

Am Tag der offenen Tür zeigte Herr Patrick Rust interessierten Gästen an seinem Stand, was sich mit, durch solcherlei Verhüttungsprozesse gewonnenem Eisen so alles schmieden lässt. Hierzu betrieb er eine kleine mobile Esse. Dort schmiedete er unter anderem Pfeil- und Speerspitzen sowie Messerklingen. Dabei griff er nicht auf die Ergebnisse von Herrn Runzheimer zurück, da beide Demonstrationen zeitgleich abliefen. Stattdessen verwendete er Amiereisen für die Pfeil- und Speerspitzen sowie Federstahl für die Messerklingen. Mit seinem Stand demonstrierte Herr Rust die Techniken der römischen Kaiserzeit (27 v. Chr. bis 284 n. Chr.).

Zuletzt sei noch einmal ein herzlicher Dank an die Zeiteninsel für die Möglichkeit der Beteiligung in Form des Informationsstandes und die Verpflegung der Beteiligten am »Aufbautag« ausgesprochen.

<!-- Fotogalerie 3 -->
{{< gallery >}}
{{<figure link="images/2018-05-13-akarchwiss-zeiteninsel-2018-rennofen-7.jpg"
  title="Luppe und Schlacke"
  caption="Ganz unten kann ein großes Stück Luppe und Schlacke geborgen werden." >}}
{{<figure link="images/2018-05-13-akarchwiss-zeiteninsel-2018-luppe.jpg"
  caption="Dieses wird sofort behauen, damit die Schlacke weitgehend abgetrennt wird." >}}
{{<figure link="images/2018-05-13-akarchwiss-zeiteninsel-2018-schmiedewerkzeug.jpg"
  title="Schmieden"
  caption="Der Schmied wählt sein Werkzeug. Er muss die Luppe letztlich noch ausschmieden, um verwertbares Eisen zu erhalten." >}}
{{<figure link="images/2018-05-13-akarchwiss-zeiteninsel-2018-schmieden.jpg"
  caption="Hier sieht man Herrn Patrick Rust, der parallel zum Rennofenbau die Schmiedetechniken der Römischen Kaiserzeit präsentierte. Er schmiedete nicht mit den Ergebnissen von Herrn Runzheimer." >}}
{{<figure link="images/2018-05-13-akarchwiss-zeiteninsel-2018-esse.jpg"
  caption="Auch die Esse braucht eine regelmäßige Luftzufuhr. Hier mit authentischem Blasebalg." >}}
{{<figure link="images/2018-05-13-akarchwiss-zeiteninsel-2018-rennofen-8.jpg"
  caption="Am Ende bleibt nicht mehr vom Rennofen übrig als ein Haufen Einzelteile." >}}
{{< /gallery >}}
<!-- Fotogalerie 3 Ende -->


<!-- Fußnoten -->
[^Schlacke]: Die nichtmetallischen Rückstände, die im Verlaufe des Verhüttungsprozess anfallen.
[^taubes Gestein]: Gestein, das kaum Erze enthält.
```
